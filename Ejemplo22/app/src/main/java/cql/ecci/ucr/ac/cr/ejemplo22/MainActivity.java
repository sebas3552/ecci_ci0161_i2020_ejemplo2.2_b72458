package cql.ecci.ucr.ac.cr.ejemplo22;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView mList;
    private List<Tip> mTips;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mList = findViewById(R.id.list);

        mTips = new ArrayList<Tip>();

        mTips.add(new Tip("Agua", "01", "Al menos 8 vasos al día"));
        mTips.add(new Tip("Vino", "02", "No exceda una copa al día"));
        mTips.add(new Tip("Café", "03", "Evite tomarlo"));
        mTips.add(new Tip("Carnes", "04", "Al menos tres veces a la semana"));
        mTips.add(new Tip("Hamburguesas", "05", "Solo caseras y bajas en grasa"));

        ArrayAdapter<Tip> adapter = new ArrayAdapter<Tip>(this, android.R.layout.simple_list_item_1, android.R.id.text1, mTips);

        mList.setAdapter(adapter);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Tip item = (Tip) mList.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "Position: " + position + " ListItem: "
                        + item.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
